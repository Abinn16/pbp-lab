import 'dart:html';

import 'package:flutter/material.dart';
import 'package:lab_7/login_page.dart';
import 'package:lab_7/register_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      theme: ThemeData(
        fontFamily: 'Georgia',
      ),
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
    );
  }
}
