from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
  class Meta():
    model = Note
    fields = ['to', 'froms', 'title', 'messages']
    widgets={
      'to':forms.TextInput(attrs={'class':'form-control'}),
      'froms': forms.TextInput(attrs={'class':'form-control'}),
      'title': forms.TextInput(attrs={'class':'form-control'}),
      'messages': forms.TextInput(attrs={'class':'form-control'})
    }