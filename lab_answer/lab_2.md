# Jawaban Pertanyaan pada Lab 2
### 1. Apakah perbedaan antara JSON dan XML?
JSON(JavaScript object notation) merupakan format data untuk menyimpan informasi dengan cara yang terorganisir dan mudah diakses.JSON menawarkan kumpulan data yang dapat dibaca manusia yang dapat diakses secara logis. Ekstensi file JSON adalah .json. JSON mengambil pendekatan sederhana untuk merepresentasikan data struktur tanpa notasi dan algoritme matematika yang rumit, plus mudah dipelajari yang menjadikannya cara yang ideal untuk membuat halaman yang lebih interaktif.

XML(Extensible Markup Language) adalah bahasa markup yang dirancang untuk menyimpan data. Case sensitive pada huruf besar/kecil. XML menawarkan kalian untuk menentukan elemen markup dan menghasilkan bahasa markup yang disesuaikan. Unit dasar dalam XML dikenal sebagai elemen. Ekstensi file XML adalah .xml. Keuntungan utama XML adalah platform itu independen yang berarti pengguna dapat mengambil data dari program lain seperti SQL dan mengubahnya menjadi XML kemudian membagikan data dengan platform lain.

Perbedaan JSON dan XML
- JSON hanyalah format yang ditulis dalam JavaScript, XML adalah bahasa markup
- Data JSON disimpan seperti map dengan pasangan key value, XML disimpan sebagai tree structure
- JSON mendukung penyandiaksaraan UTF serta ASCII, XML mendukung pengkodean UTF-8 dan UTF-16.
- JSON memiliki ekstensi .json, XML memiliki ekstensi .xml

### 2. Apakah perbedaan antara HTML dan XML?
HTML (HyperText Markup Language) adalah bahasa markup yang digunakan untuk membuat halaman web dan aplikasi web. Ini adalah bahasa komputer yang digunakan untuk menerapkan tata letak dan konvensi pemformatan ke dokumen teks. 

XML(Extensible Markup Language) adalah bahasa markup yang dirancang untuk menyimpan data. Case sensitive pada huruf besar/kecil. XML menawarkan kalian untuk menentukan elemen markup dan menghasilkan bahasa markup yang disesuaikan. Unit dasar dalam XML dikenal sebagai elemen.

Perbedaan XML dan HTML
- XML berfokus pada transfer data sedangkan HTML difokuskan pada penyajian data.
- XML didorong konten sedangkan HTML didorong oleh format.
- XML itu Case Sensitive sedangkan HTML Case Insensitive
- Tag XML dapat dikembangkan sedangkan HTML memiliki tag terbatas.
