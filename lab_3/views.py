from django.shortcuts import render
from lab_1.models import Friend
from .forms import FriendForm 
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
  friends = Friend.objects.all().values
  response ={'friends': friends}
  return render(request,'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friends(request):
  form = FriendForm(request.POST)
  friends = Friend.objects.all().values
  response ={'friends': friends}
  
  if(form.is_valid() and request.method == 'POST'):
    form.save()
    return render(request, 'lab3_index.html',response)
  
  return render(request, 'lab3_form.html',{'form':form})