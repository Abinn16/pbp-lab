from django import forms
from lab_1.models import Friend
from django.contrib.admin.widgets import AdminDateWidget
class DateInput(forms.DateInput):
  input_type = 'date'

class FriendForm(forms.ModelForm):
  class Meta:
    model = Friend
    fields =['name','npm','date']
    widgets ={
      'date' :DateInput()
    }

 
  