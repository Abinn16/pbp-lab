from django.urls import path
from .views import index
from .views import add_friends

urlpatterns = [
  path('',index,name='index'),
  path('add',add_friends, name='add_friends')
]