from django.shortcuts import render
from lab_2.models import Note
# Create your views here.
def index(request):
    notes = Note.objects.all().values()  # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab2.html', response)